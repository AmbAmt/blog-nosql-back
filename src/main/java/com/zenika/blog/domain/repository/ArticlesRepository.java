package com.zenika.blog.domain.repository;

import com.zenika.blog.domain.model.articles.Articles;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface ArticlesRepository extends MongoRepository<Articles, String> {
    Optional<Articles> findByTitle(String articleTitle);
}
