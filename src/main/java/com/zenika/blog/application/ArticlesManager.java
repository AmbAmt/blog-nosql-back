package com.zenika.blog.application;

import com.zenika.blog.domain.model.articles.Articles;
import com.zenika.blog.domain.repository.ArticlesRepository;
import com.zenika.blog.web.articles.CreateArticlesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Optional;
import java.util.UUID;

@Component
public class ArticlesManager {
    private final ArticlesRepository articlesRepository;

    @Autowired
    public ArticlesManager(ArticlesRepository articlesRepository){
        this.articlesRepository = articlesRepository;
    }

    public Articles createArticle(String title, String summary,String content){
        Articles articles = new Articles(UUID.randomUUID().toString(),title,summary,content);
        articlesRepository.save(articles);
        return articles;
    }

    public Iterable<Articles> getAllArticles(){
        return articlesRepository.findAll();
    }

    public Optional<Articles> getArticle(String articleTid){
        return articlesRepository.findById(articleTid);
    }

    public Optional<Articles> getArticleByTitle(String articleTitle){return  articlesRepository.findByTitle(articleTitle);}

    public Articles changeArticle(String articleTid,String title,String content, String summary,String tags) throws CreateArticlesDto.ArticleNotFoundException {
        //fonctionne que si c'est draft
        Articles articles = articlesRepository.findById(articleTid).orElseThrow(CreateArticlesDto.ArticleNotFoundException::new);
        articles.setTitle(title);
        articles.setContent(content);
        articles.setSummary(summary);
        articlesRepository.save(articles);
        return articles;
    }

    public void deleteArticle(String articleTid){
        articlesRepository.deleteById(articleTid);
    }
}
