package com.zenika.blog.web.articles;

public record ModifyArticlesDto(String title,String content, String summary,String tags) {
}
