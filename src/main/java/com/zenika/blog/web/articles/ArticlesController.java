package com.zenika.blog.web.articles;

import com.zenika.blog.application.ArticlesManager;
import com.zenika.blog.domain.model.articles.Articles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/articles")
public class ArticlesController {

    private final ArticlesManager articlesManager;

    @Autowired
    public ArticlesController(ArticlesManager articlesManager){
        this.articlesManager = articlesManager;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Articles createArticle(@RequestBody CreateArticlesDto articlesDto){
        return articlesManager.createArticle(articlesDto.title(),
                 articlesDto.summary(),articlesDto.content());
    }
    @GetMapping
    ResponseEntity<Iterable<Articles>> getAllArticles(){
        return ResponseEntity.ok(articlesManager.getAllArticles());
    }

    @GetMapping("/{articleTid}")
    ResponseEntity<Articles> getArticle(@PathVariable String articleTid){
        return articlesManager.getArticle(articleTid).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @GetMapping("/")
    ResponseEntity<Articles> getArticleByTitle(@RequestParam(name="title") String articleTitle){
        return  articlesManager.getArticleByTitle(articleTitle).map(ResponseEntity::ok).orElseGet(()->ResponseEntity.notFound().build());
    }

    @PatchMapping("/{articleTid}")
    ResponseEntity<Articles> changeArticle(@PathVariable String articleTid, @RequestBody ModifyArticlesDto modifyArticlesDto){
        try {
            Articles articles = articlesManager.changeArticle(articleTid, modifyArticlesDto.title(), modifyArticlesDto.content(), modifyArticlesDto.summary(), modifyArticlesDto.tags());
            return ResponseEntity.ok(articles);
        } catch (CreateArticlesDto.ArticleNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{articleTid}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteArticle(@PathVariable String articleTid){
        //mettre en place la vérification que l'utilisateur supprimant l'article est bien le créateur
        articlesManager.deleteArticle(articleTid);
    }

}
